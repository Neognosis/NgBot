﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace NgBot
{
    public class HelpCommand : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        [Summary("PMs you this list.")]
        public async Task Help()
        {
            EmbedBuilder builder = new EmbedBuilder()
                .WithTitle("BallisticNG Bot Commands");

            foreach (ModuleInfo module in Program.Commands.Modules)
            {
                string description = null;
                foreach (CommandInfo command in module.Commands)
                {
                    PreconditionResult result = await command.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess) description += $"__***{command.Aliases.First()}***__ - {command.Summary}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            IDMChannel channel = await Context.User.GetOrCreateDMChannelAsync();
            await channel.SendMessageAsync("", false, builder.Build());
        }
    }

    public class RoleCommands : ModuleBase<SocketCommandContext>
    {
        public SocketRole DeveloperRole => Context.Guild.Roles.FirstOrDefault(r => r.Name == "Developer");

        [Command("changelog")]
        [Summary("Toggles the beta changelog role. You'll be pinged for beta branch releases with this role.")]
        public async Task AddToChangelog()
        {
            await ToggleRole("Changelog");
        }

        [Command("multiplayer")]
        [Summary("Toggles the multiplayer role. With this on you can be pinged by people looking for others to play with or alerting others that they're hosting.")]
        public async Task AddToMultiplayer()
        {
            await ToggleRole("Multiplayer");
        }
        
        [Command("timetrial")]
        [Summary("Toggles the time trial role. With this on you can be pinged by people managing custom time trial events.")]
        public async Task AddToTimeTrial()
        {
            await ToggleRole("Time Trial");
        }

        /// <summary>
        /// Sets the role of the user in the current context.
        /// </summary>
        public async Task ToggleRole(string roleName)
        {
            SocketRole role = Context.Guild.Roles.FirstOrDefault(r => r.Name == roleName);
            if (role == null)
            {
                await Context.Channel.SendMessageAsync($"ERROR: {roleName} role not found! {DeveloperRole?.Mention ?? ""}");
                return;
            }

            IGuildUser user = Context.User as IGuildUser;
            if (user.RoleIds.Contains(role.Id))
            {
                await user.RemoveRoleAsync(role);
                await Context.Channel.SendMessageAsync($"Removed you from the {roleName} role.");
            }
            else
            {
                await user.AddRoleAsync(role);
                await Context.Channel.SendMessageAsync($"Added you to the {roleName} role.");
            }
        }

        [Command("team")]
        [Summary("Changes your team role. Use **remove** to remove your team role, **list** to get a list of the team roles and **teamname** to set the role.")]
        public async Task SetTeam(string newRole)
        {
            /*---Do nothing if the user hasn't requested a role---*/
            if (string.IsNullOrEmpty(newRole))
            {
                await Context.Channel.SendMessageAsync("Please provide a team! Use List param for a list of available teams.");
                return;
            }

            /*---Make sure first character is upper---*/
            char[] roleArr = newRole.ToCharArray();
            roleArr[0] = char.ToUpper(roleArr[0]);
            newRole = new string(roleArr);

            /*---Handle list request---*/
            if (newRole == "List")
            {
                StringBuilder list = new StringBuilder("");
                for (int i = 0; i < Database.TeamRoles.Length; ++i) list.Append(i == Database.TeamRoles.Length - 1 ? Database.TeamRoles[i] : $"{Database.TeamRoles[i]}, ");
                await Context.Channel.SendMessageAsync(list.ToString());
                return;
            }

            /*---Handle remove request---*/
            if (newRole == "Remove")
            {
                IGuildUser remUser = Context.User as IGuildUser;
                SocketRole[] remRoles = Context.Guild.Roles.Where(r => Database.TeamRoles.Contains(r.Name)).ToArray();

                foreach (SocketRole role in remRoles)
                {
                    if (remUser.RoleIds.Contains(role.Id)) await remUser.RemoveRoleAsync(role);
                }
                await Context.Channel.SendMessageAsync("Removed your team role.");
                return;
            }

            /*---Make sure the requested team is a registered team---*/
            bool isTeam = Database.TeamRoles.Contains(newRole);
            if (!isTeam)
            {
                await Context.Channel.SendMessageAsync("Please provide a valid team! Use List param for a list of available teams.");
                return;
            }

            /*---Make sure the role exists---*/
            SocketRole targetRole = Context.Guild.Roles.FirstOrDefault(r => r.Name == newRole);
            if (targetRole == null)
            {
                await Context.Channel.SendMessageAsync($"ERROR: Requested role doesn't exist! (this is a problem on the bots end). {DeveloperRole?.Mention ?? ""}");
                return;
            }

            /*
             * Now with all of the initial error checking out of the way, we want to iterate over the team roles array and use each entry to grab the team role.
             * After this we can remove all of the team roles from the user and add the one they want.
             */
            IGuildUser user = Context.User as IGuildUser;
            SocketRole[] roles = Context.Guild.Roles.Where(r => Database.TeamRoles.Contains(r.Name)).ToArray();

            foreach (SocketRole role in roles)
            {
                if (user.RoleIds.Contains(role.Id)) await user.RemoveRoleAsync(role);
            }
            await user.AddRoleAsync(targetRole);
            await Context.Channel.SendMessageAsync($"Set your team role to {targetRole.Name}.");
        }
    }
}
