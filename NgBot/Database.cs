﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgBot
{
    public class Database
    {
        /// <summary>
        /// The name of the server the bot is targeting.
        /// </summary>
        internal static string ServerName = "BallisticNG";
        /// <summary>
        /// The name of the game to look for when applying PlayingGameRole.
        /// </summary>
        internal static string GameName = "BallisticNG";
        /// <summary>
        /// The name of the channel that the bot listens to.
        /// </summary>
        internal static string TargetChannel = "ngbot";

        /// <summary>
        /// The name of the Playing Game role on the server.
        /// </summary>
        internal static string PlayingGameRole = "Playing BnG";

        /// <summary>
        /// The name of the team roles present on the server.
        /// </summary>
        internal static string[] TeamRoles = {"G-Tek", "Omnicom", "Nexus", "Diavolt", "Tenrai", "Wyvern", "Scorpio", "Hyperion", "Barracuda", "M-Tech", "Protonic", "Caliburn", "NX2000", "Precision Delta", "Orbitronix"};
    }
}
